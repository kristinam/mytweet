package com.example.cexwaterford.tweet.app;

import android.app.Application;
import com.example.cexwaterford.tweet.models.Portfolio;
import com.example.cexwaterford.tweet.models.PortfolioSerializer;

import static com.example.cexwaterford.android.helpers.LogHelpers.info;


public class TweetApp extends Application
{
    private static final String FILENAME = "portfolio.json";
    public Portfolio portfolio;

    @Override
    public void onCreate()
    {
        super.onCreate();
        PortfolioSerializer serializer = new PortfolioSerializer(this, FILENAME);
        portfolio = new Portfolio(serializer);

        info(this, "Tweet app launched");
    }
}
