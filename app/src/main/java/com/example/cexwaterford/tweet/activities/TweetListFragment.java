package com.example.cexwaterford.tweet.activities;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.ActionMode;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import com.example.cexwaterford.android.helpers.IntentHelper;
import com.example.cexwaterford.tweet.R;
import com.example.cexwaterford.tweet.app.TweetApp;
import com.example.cexwaterford.tweet.models.Portfolio;
import com.example.cexwaterford.tweet.models.Tweet;

import java.util.ArrayList;

//import android.app.ListFragment;


public class TweetListFragment extends ListFragment implements OnItemClickListener, AbsListView.MultiChoiceModeListener {
    private ArrayList<Tweet> tweets;
    private Portfolio portfolio;
    private TweetAdapter adapter;

    private ListView listView;
    private static final int SETTINGS_RESULT = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        getActivity().setTitle(R.string.app_name2);

        TweetApp app = (TweetApp) getActivity().getApplication();
        portfolio = app.portfolio;
        tweets = portfolio.tweets;

        adapter = new TweetAdapter(getActivity(), tweets);
        setListAdapter(adapter);
        adapter.notifyDataSetChanged();

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        View v = super.onCreateView(inflater, parent, savedInstanceState);

        listView=(ListView)v.findViewById(android.R.id.list);
        listView.setChoiceMode(ListView.CHOICE_MODE_MULTIPLE_MODAL);
        listView.setMultiChoiceModeListener(this);

        return v;
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id)
    {
        Tweet tw = ((TweetAdapter) getListAdapter()).getItem(position);
        Intent i = new Intent(getActivity(), TweetPagerActivity.class);
        i.putExtra(TweetFragment.EXTRA_TWEET_ID, tw.id);
        startActivityForResult(i, 0);



    }

    @Override
    public void onResume()
    {
        super.onResume();
        ((TweetAdapter) getListAdapter()).notifyDataSetChanged();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater)
    {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.tweetlist, menu);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.menu_item_new_tweet:
                Tweet tweet = new Tweet();
                portfolio.addTweet(tweet);

                Intent i = new Intent(getActivity(), TweetPagerActivity.class);
                i.putExtra(TweetFragment.EXTRA_TWEET_ID, tweet.id);
                startActivityForResult(i, 0);
                return true;

            case R.id.menuClear:
                //adapter.clear();
                portfolio.clearList();
                adapter.notifyDataSetChanged();
                return true;

            case R.id.preferences:
            {
                i = new Intent(getActivity(), MyPreferenceActivity.class);
                startActivityForResult(i, SETTINGS_RESULT);
                return true;

            }

            default:
                return super.onOptionsItemSelected(item);
        }
        }

    /* ************ MultiChoiceModeListener methods (begin) *********** */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id)
    {
        Tweet tweet = adapter.getItem(position);
        IntentHelper.startActivityWithData(getActivity(), TweetPagerActivity.class, "TWEET_ID", tweet.id);


    }

    @Override
    public void onItemCheckedStateChanged(ActionMode actionMode, int i, long l, boolean b) {

    }

    @Override
    public boolean onCreateActionMode(ActionMode actionMode, Menu menu)
    {
        MenuInflater inflater = actionMode.getMenuInflater();
        inflater.inflate(R.menu.tweet_list_context, menu);
        return true;
    }

    @Override
    public boolean onPrepareActionMode(ActionMode actionMode, Menu menu)
    {
        return false;
    }

    @Override
    public boolean onActionItemClicked(ActionMode actionMode, MenuItem menuItem) {
        switch (menuItem.getItemId()) {
            case R.id.menu_item_delete_tweet:
                deleteTweet(actionMode);
                return true;

            default:return false;
        }

    }

    @Override
    public void onDestroyActionMode(ActionMode mode) {

    }

    private void deleteTweet(ActionMode actionMode)
    {
        for (int i = adapter.getCount() - 1; i >= 0; i--)
        {
            if (listView.isItemChecked(i))
            {
                portfolio.deleteTweet(adapter.getItem(i));
            }
        }
        actionMode.finish();
        adapter.notifyDataSetChanged();
    }
    /* ************ MultiChoiceModeListener methods (end) *********** */

    class TweetAdapter extends ArrayAdapter<Tweet>
    {
        private Context context;

        public TweetAdapter(Context context, ArrayList<Tweet> tweets)
        {
            super(context, 0, tweets);
            this.context = context;
        }

        @SuppressLint("InflateParams")
        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            if (convertView == null)
            {
                convertView = inflater.inflate(R.layout.list_item_tweet, null);
            }
            Tweet tw = getItem(position);

            TextView message_text = (TextView) convertView.findViewById(R.id.tweet_list_item_message_text);
            message_text.setText(tw.message_text);

            TextView dateTextView = (TextView) convertView.findViewById(R.id.tweet_list_item_dateTextView);
            dateTextView.setText(tw.getDateString());


            return convertView;
        }
    }
}