package com.example.cexwaterford.tweet.models;

import android.content.Context;

import com.example.cexwaterford.tweet.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;


public class Tweet {
    public UUID id;
    public Date date;
    public String message_text;
    public String contact;

    private static final String JSON_ID = "id";
    private static final String JSON_MESSAGE_TEXT = "message_text";
    private static final String JSON_DATE = "date";
    private static final String JSON_CONTACT         = "contact";

    public Tweet() {
        this.id = UUID.randomUUID();
        this.date = new Date();
        this.message_text = "hello";
        this.contact      = ": none presently";
    }

    public Tweet(JSONObject json) throws JSONException
    {
        id             = UUID.fromString(json.getString(JSON_ID));
        message_text   = json.getString(JSON_MESSAGE_TEXT);
        date           = new Date(json.getLong(JSON_DATE));
        contact        = json.getString(JSON_CONTACT);


    }

    public JSONObject toJSON() throws JSONException
    {
        JSONObject json = new JSONObject();
        json.put(JSON_ID            , id.toString());
        json.put(JSON_MESSAGE_TEXT  , message_text);
        json.put(JSON_DATE          , date.getTime());
        json.put(JSON_CONTACT       , contact);
        return json;
    }

    public String getDateString()
    {
        return "Sent: " + DateFormat.getDateTimeInstance().format(date);
    }

    public void editTweet(String message_text)

    {
        this.message_text = message_text;
    }

    public String getTweet(Context context)
    {
        String dateFormat = "EEE, MMM dd";
        String dateString = android.text.format.DateFormat.format(dateFormat, date).toString();
        String friendContact = contact;

        if (contact == null)
        {
            friendContact = context.getString(R.string.no_contacts);
        }
        else
        {
            friendContact = context.getString(R.string.contact, contact);
        }
        //String report =  "text: " + message_text + " Date: " + dateString;
        String report =  message_text;
        return report;
    }


}




