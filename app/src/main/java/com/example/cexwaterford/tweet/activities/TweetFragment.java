package com.example.cexwaterford.tweet.activities;

/**
 * Created by Cex Waterford on 25/10/2015.
 */

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.Fragment;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.cexwaterford.android.helpers.ContactHelper;
import com.example.cexwaterford.tweet.R;
import com.example.cexwaterford.tweet.app.TweetApp;
import com.example.cexwaterford.tweet.models.Portfolio;
import com.example.cexwaterford.tweet.models.Tweet;

import java.text.DateFormat;
import java.util.Date;
import java.util.UUID;

import static com.example.cexwaterford.android.helpers.IntentHelper.navigateUp;
import static com.example.cexwaterford.android.helpers.IntentHelper.sendEmail;


public class TweetFragment extends Fragment implements TextWatcher,
        OnClickListener {

    public static   final String  EXTRA_TWEET_ID = "tweet.TWEET_ID";
    private static final int REQUEST_CONTACT = 1;

    private Portfolio portfolio;
    private Tweet tweet;

    private TextView date;
    private EditText message_text;
    private TextView countdown;
    private Button tweet_button;
    private Button select_contact;
    private Button email_tweet;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

        UUID tweetId = (UUID)getArguments().getSerializable(EXTRA_TWEET_ID);


        TweetApp app = (TweetApp) getActivity().getApplication();
        portfolio = app.portfolio;
        tweet = portfolio.getTweet(tweetId);

    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup parent, Bundle savedInstanceState)
    {
        super.onCreateView(inflater, parent, savedInstanceState);
        View v = inflater.inflate(R.layout.fragment_tweet, parent, false);

        getActivity().getActionBar().setDisplayHomeAsUpEnabled(true);
        addListeners(v);
        updateControls(tweet);

        return v;
    }

    private void addListeners(View v)
    {
        message_text = (EditText) v.findViewById(R.id.message_text);
        date  = (TextView)   v.findViewById(R.id.date);
        String currentDateTimeString = DateFormat.getDateTimeInstance().format(new Date());
        select_contact = (Button)  v.findViewById(R.id.select_contact);
        email_tweet = (Button) v.findViewById(R.id.email_tweet);
        countdown = (TextView) v.findViewById(R.id.countdown);
        tweet_button = (Button) v.findViewById(R.id.tweet_button);

        message_text.addTextChangedListener(this);
        date.setText(currentDateTimeString);
        select_contact.setOnClickListener(this);
        email_tweet.setOnClickListener(this);
        tweet_button.setOnClickListener(this);

    }

    public void updateControls(Tweet tweet)
    {
        message_text.setText(tweet.message_text);
        date.setText(tweet.getDateString());
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        switch (item.getItemId())
        {
            case android.R.id.home: navigateUp(getActivity());
                return true;
            default:                return super.onOptionsItemSelected(item);

        }
    }

    @Override
    public void onPause()
    {
        super.onPause();
        portfolio.saveTweets();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        if (resultCode != Activity.RESULT_OK)
        {
            return;
        }
        else
        if (requestCode == REQUEST_CONTACT)
        {
            String name = ContactHelper.getContact(getActivity(), data);
            tweet.contact = name;
            select_contact.setText(name);
        }
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after)
    { }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count)
    {}

    @Override
    public void afterTextChanged(Editable editable)
    {
        Log.i(this.getClass().getSimpleName(), "tweet_text" + editable.toString());
        tweet.message_text = editable.toString();
        countdown.setText(String.valueOf(140 - editable.length()));
        tweet.editTweet(editable.toString());

    }


    @Override
    public void onClick(View v)
    {
        switch (v.getId())
        {
            case R.id.select_contact                 : Intent i = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
                startActivityForResult(i, REQUEST_CONTACT);
                if (tweet.contact != null) {
                    select_contact.setText("Contact: "+tweet.contact);
                }
                break;


            case R.id.tweet_button:
                Toast toast = Toast.makeText(getActivity(), "Message Sent", Toast.LENGTH_SHORT);
                toast.show();
                break;
            case R.id.email_tweet : sendEmail(getActivity(), "", getString(R.string.tweet_email_subject), tweet.getTweet(getActivity()));
                break;

        }
    }



}


